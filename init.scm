(import (scheme base)
        (scheme write)
        (retropikzel spite v0.1.0 main))

(spite-init "Spite Hello World" 400 400)
(spite-reload-allow "R" "main.scm")

(define black (color-make 0 0 0 255))
(define rainbow-lambda (spite-image-load "rainbow-lambda-logo.png"))
(define font (spite-font-load "RETRO_SPACE.ttf" 24))
(define click (spite-audio-load "click.wav"))

(define logo-x 200)
(define logo-y 200)

(time-interval
  (lambda ()
    (display "Hello interval world")
    (newline))
  2000)
