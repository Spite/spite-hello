;> # Spite
;>
(define-library
  (retropikzel spite v0.1.0 main)
  (import (scheme base)
          (scheme write)
          (scheme complex)
          (scheme process-context)
          (scheme file)
          (scheme load)
          (retropikzel pffi v0.1.0 main)
          (retropikzel event v0.1.0 main)
          (retropikzel time v0.1.0 main)
          (retropikzel color v0.1.1 main)
          (retropikzel geometry v0.1.0 main)
          (retropikzel vector-2d v0.2.0 main))
  (export spite-init
          spite-start
          spite-started?
          spite-update
          spite-draw
          event-make
          event-type-get
          event-data-get
          event-push
          event-clear
          spite-image-load
          spite-image-draw
          spite-image-draw-slice
          spite-point-draw
          spite-line-draw
          spite-rectangle-draw
          spite-font-load
          spite-text-draw
          spite-audio-load
          spite-audio-play
          spite-window-resizing-allow
          spite-window-resizing-disallow
          spite-renderer-size-set
          spite-reload-allow
          color-make
          color?
          color-red-get
          color-red-set!
          color-green-get
          color-green-set!
          color-blue-get
          color-blue-set!
          color-alpha-get
          color-alpha-set!
          color->vector
          time-as-milliseconds
          time-as-seconds
          time-interval
          time-timeout
          geometry-point-make
          geometry-point?
          geometry-point-x-get
          geometry-point-x-set!
          geometry-point-y-get
          geometry-point-y-set!
          geometry-point-inside-rectangle?
          geometry-line-make
          geometry-line?
          geometry-line-a-get
          geometry-line-a-set!
          geometry-line-b-get
          geometry-line-b-set!
          geometry-rectangle-make
          geometry-rectangle?
          geometry-rectangle-a-get
          geometry-rectangle-a-set!
          geometry-rectangle-b-get
          geometry-rectangle-b-set!
          geometry-rectangle-c-get
          geometry-rectangle-c-set!
          geometry-rectangle-d-get
          geometry-rectangle-d-set!
          geometry-rectangle-position-get
          geometry-rectangle-position-set!
          geometry-rectangle-x-get
          geometry-rectangle-y-get
          geometry-rectangle-width-get
          geometry-rectangle-height-get
          vector-2d-make
          vector-2d?
          vector-2d-width-get
          vector-2d-height-get
          vector-2d-width-set!
          vector-2d-height-set!
          vector-2d-cells-get
          vector-2d-cells-set!
          vector-2d-get
          vector-2d-set!
          vector-2d-display
          )
  (begin

    ;; * at the end of name marks a pffi pointer in global scope

    (define version "v0.1.0")
    (define started? #f)
    (define error? #f)
    (define main-file-path "main.scm")
    (define reload-key #f)
    (define scale-x 1.0)
    (define scale-y 1.0)
    (define slash (cond-expand (windows (string #\\)) (else "/")))
    (define additional-load-paths
      (list (string-append "retropikzel"
                           slash
                           "spite"
                           slash
                           version)
            (string-append "schubert"
                           slash
                           "retropikzel"
                           slash
                           "spite"
                           slash
                           version)))
    (define sdl2* #f)
    (define sdl2-image* #f)
    (define sdl2-ttf* #f)
    (define sdl2-mixer* #f)
    (define window* #f)
    (define renderer* #f)
    (define event* (pffi-pointer-allocate 4000))
    (define image-draw-rect* (pffi-pointer-allocate (* (pffi-size-of 'int) 4)))
    (define image-draw-slice-rect* (pffi-pointer-allocate (* (pffi-size-of 'int) 4)))
    (define draw-text-draw-rect* (pffi-pointer-allocate (* (pffi-size-of 'int) 4)))
    (define draw-text-text-width* (pffi-pointer-allocate (pffi-size-of 'int)))
    (define draw-text-text-height* (pffi-pointer-allocate (pffi-size-of 'int)))
    (define draw-text-color* (pffi-pointer-allocate (* (pffi-size-of 'uint8) 125)))
    (define null* (pffi-pointer-null))

    (define update-procedure #f)
    (define draw-procedure #f)

    (define images (vector))
    (define fonts (vector))
    (define audios (vector))

    ;> ## Events
    ;>

    ;> ### key-down
    ;>
    ;> Arguments:
    ;>
    ;> - key (string)
    ;>   - The name of the pressed key as a string
    ;>   - scancode (number)
    ;>     - The scancode of the pressed key
    ;> - repeat? (boolean)
    ;>   - If the key press is a repeat (It's hold down)
    ;>

    ;> ### key-up
    ;>
    ;> Arguments:
    ;>
    ;> - key (string)
    ;>   - The name of the pressed key as a string
    ;>   - scancode (number)
    ;>     - The scancode of the pressed key
    ;> - repeat? (boolean)
    ;>   - If the key press is a repeat (It's hold down)
    ;>

    ;> ### mouse-moved
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;>

    ;> ### mouse-button-up
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;> - button (number)
    ;>   - The index of a button that went down
    ;> - clicks
    ;>   - The count of clicks, > 1 means doubleclick for example
    ;>

    ;> ### mouse-button-down
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;>   - X position of the mouse inside the window
    ;> - y (number)
    ;>   - Y position of the mouse inside the window
    ;> - button (number)
    ;>   - The index of a button that went down
    ;> - clicks
    ;>   - The count of clicks, > 1 means doubleclick for example
    ;>

    ;> ## Procedures
    ;>

    ;> ### spite-update
    ;>
    ;> Arguments:
    ;>
    ;> - proc (procedure)
    ;>   - Procedure to run on the games main loop before drawing
    ;>
    ;> Example:
    ;>
    ;> (spite-update (lambda () (display "Updating...") (newline))
    (define spite-update (lambda (proc) (set! update-procedure proc)))

    ;> ### spite-draw
    ;>
    ;> Arguments:
    ;>
    ;> - proc (procedure)
    ;>   - Procedure inside which all games drawing should happen
    ;>
    ;> Example:
    ;>
    ;> (spite-draw (lambda () (display "Drawing...") (newline)))
    (define spite-draw (lambda (proc) (set! draw-procedure proc)))

    (define main-loop
      (lambda ()
        (call-with-current-continuation
          (lambda (k)
            (if (not error?)
              (with-exception-handler
                (lambda (x)
                  (cond-expand
                    (kawa
                      (write x)
                      (newline)
                      (invoke x 'printStackTrace (current-error-port)))
                    (else
                      (display "Error: ")
                      (newline)
                      (write (error-object-message x))
                      (newline)
                      (write (error-object-irritants x))
                      (newline)
                      (write x)
                      (newline)))
                  (set! error? #t)
                  (k))
                (lambda ()
                  (time-update)
                  (sdl2-get-events)
                  (if update-procedure (update-procedure (event-poll)))
                  (render-clear)
                  (if draw-procedure (draw-procedure))
                  (render-present)))
              (sdl2-get-events))))
        (main-loop)))

    (define sdl2-event->spite-event
      (lambda (event)
        (let ((type (pffi-pointer-get event 'uint32 0)))
          (cond
            ((= type 256)
             (let ((type 'quit))
               (list (cons 'type type))))
            ((or (= type 768) (= type 769))
             (let*
               ((type (if (= type 768) 'key-down 'key-up))
                (scancode (pffi-pointer-get event
                                            'int
                                            (+ (* (pffi-size-of 'uint32) 3)
                                               (* (pffi-size-of 'uint8) 4))))
                (keycode (pffi-call sdl2*
                                    'SDL_GetKeyFromScancode
                                    'int32
                                    (list (cons 'int scancode))))
                (key (pffi-pointer->string
                       (pffi-call sdl2*
                                  'SDL_GetKeyName
                                  'string
                                  (list (cons 'int32 keycode)))))
                (repeat? (= (pffi-pointer-get
                              event
                              'uint8
                              (+ (* (pffi-size-of 'uint32) 3)
                                 (pffi-size-of 'uint8))) 1)))
               (list (cons 'type type)
                     (cons 'key key)
                     (cons 'scancode scancode)
                     (cons 'repeat? repeat?))))
            ((= type 1024)
             (let ((type 'mouse-motion)
                   (x (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 5))))
                   (y (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 6)))))
               (list (cons 'type type)
                     (cons 'x x)
                     (cons 'y y))))
            ((or (= type 1025) (= type 1026))
             (let ((type (if (= type 1025) 'mouse-button-down 'mouse-button-up))
                   (x (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 4)
                                                        (* (pffi-size-of 'uint8) 4))))
                   (y (pffi-pointer-get event 'int32 (+ (* (pffi-size-of 'uint32) 4)
                                                        (* (pffi-size-of 'uint8) 4)
                                                        (pffi-size-of 'int32))))
                   (button (pffi-pointer-get event 'uint8 (+ (* (pffi-size-of 'uint32) 4))))
                   (clicks (pffi-pointer-get event 'uint8 (+ (* (pffi-size-of 'uint32) 4)
                                                             (* (pffi-size-of 'uint8) 2)))))
               (list (cons 'type type)
                     (cons 'x x)
                     (cons 'y y)
                     (cons 'button button)
                     (cons 'clicks clicks))))
            (else
              (list (cons 'type 'sdl2-event)
                    (cons 'sdl2-type-number type)))))))

    (define sdl2-get-events
      (lambda ()
        (let*
          ((poll-result (pffi-call sdl2*
                                   'SDL_PollEvent
                                   'int
                                   (list (cons 'pointer event*))))
           (event (sdl2-event->spite-event event*)))
          (cond ((and reload-key
                       (equal? (cdr (assoc 'type event)) 'key-down)
                       (string=? (cdr (assoc 'key event)) reload-key))
                   (set! error? #f)
                   (display "Reloading!")
                    (newline)
                    (spite-reload main-file-path))
                ((equal? (cdr (assoc 'type event)) 'quit) (exit)))
          (event-push (event-make (cdr (assoc 'type event)) event))
          (if (not (= poll-result 1)) (sdl2-get-events)))))


    (define render-clear
      (lambda ()
        (pffi-call sdl2*
                   'SDL_SetRenderDrawColor
                   'int
                   (list
                     (cons 'pointer renderer*)
                     (cons 'int 255)
                     (cons 'int 255)
                     (cons 'int 255)
                     (cons 'int 255)))
        (pffi-call sdl2* 'SDL_RenderClear 'int (list (cons 'pointer renderer*)))))

    (define render-present
      (lambda ()
        (pffi-call sdl2* 'SDL_RenderPresent 'int (list (cons 'pointer renderer*)))))

    ;> ### spite-image-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>   - Path to the image you want to load
    ;>
    ;> Returns: (number) Image index
    ;>
    ;> Loads image from the path, supported filetypes are same as supported by
    ;> SDL_image [https://wiki.libsdl.org/SDL2_image/FrontPage](https://wiki.libsdl.org/SDL2_image/FrontPage)
    (define spite-image-load
      (lambda (path)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load image, no such file: " path)))
        (set! images
          (vector-append
            images
            (vector (pffi-call sdl2-image*
                               'IMG_LoadTexture
                               'pointer
                               (list (cons 'pointer renderer*)
                                     (cons 'pointer (pffi-string->pointer path)))))))
        (- (vector-length images) 1)))

    ;> ### spite-image-draw
    ;>
    ;> Arguments:
    ;>
    ;> - image-index (number)
    ;>   - Index returned by spite-image-load
    ;> - x (number)
    ;>   - Images left top corner x position inside the window
    ;> - y (number)
    ;>   - Images left top corner y position inside the window
    ;> - width (number)
    ;>   - What width should the image be drawn
    ;> - height (number)
    ;>   - What height should the image be drawn
    (define spite-image-draw
      (lambda (image-index x y width height)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 0) x)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 1) y)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 2) width)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 3) height)
        (pffi-call sdl2*
                   'SDL_RenderCopy
                   'int
                   (list (cons 'pointer renderer*)
                         (cons 'pointer (vector-ref images image-index))
                         (cons 'pointer null*)
                         (cons 'pointer image-draw-rect*)))))

    ;> ### spite-image-draw-slice
    ;>
    ;> Arguments:
    ;>
    ;> - image-index (number)
    ;>   - Index returned by spite-image-load
    ;> - x (number)
    ;>   - Images left top corner x position inside the window
    ;> - y (number)
    ;>   - Images left top corner y position inside the window
    ;> - width (number)
    ;>   - What width should the slice be drawn
    ;> - height (number)
    ;>   - What height should the slice be drawn
    ;> - slice-x (number)
    ;>   - Slices top left corner x position inside the image
    ;> - slice-y (number)
    ;>   - Slices top left corner y position inside the image
    ;> - slice-height
    ;>   - Slices height
    ;> - slice-width
    ;>   - Slices width
    ;>
    ;> Slice here refers to rectangle inside the image. So instead of drawing
    ;> the whole image, only the slice is drawn
    (define spite-image-draw-slice
      (lambda (image x y width height slice-x slice-y slice-height slice-width)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 0) x)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 1) y)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 2) width)
        (pffi-pointer-set! image-draw-rect* 'int (* (pffi-size-of 'int) 3) height)
        (pffi-pointer-set! image-draw-slice-rect* 'int (* (pffi-size-of 'int) 0) slice-x)
        (pffi-pointer-set! image-draw-slice-rect* 'int (* (pffi-size-of 'int) 1) slice-y)
        (pffi-pointer-set! image-draw-slice-rect* 'int (* (pffi-size-of 'int) 2) slice-width)
        (pffi-pointer-set! image-draw-slice-rect* 'int (* (pffi-size-of 'int) 3) slice-height)
        (pffi-call sdl2*
                   'SDL_RenderCopy
                   'int
                   (list (cons 'pointer renderer*)
                         (cons 'pointer (vector-ref images image))
                         (cons 'pointer image-draw-slice-rect*)
                         (cons 'pointer image-draw-rect*)))))

    ;> ### spite-point-draw
    ;> Arguments:
    ;> - point (geometry-point) Point you want to draw
    ;> - size (number) How big the point should be
    ;> - color (color) The color of the point
    (define spite-point-draw
      (lambda (point size color)
        (let ((x (geometry-point-x-get point))
              (y (geometry-point-y-get point)))
          (pffi-call sdl2*
                     'SDL_SetRenderDrawColor
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (color-red-get color))
                       (cons 'int (color-green-get color))
                       (cons 'int (color-blue-get color))
                       (cons 'int (color-alpha-get color))))
          (spite-renderer-scale-set size size)
          (pffi-call sdl2*
                     'SDL_RenderDrawLine
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (exact (round (/ x size))))
                       (cons 'int (exact (round (/ y size))))
                       (cons 'int (exact (round (/ x size))))
                       (cons 'int (exact (round (/ y size))))))
          (spite-renderer-scale-set scale-x scale-y))))

    ;> ### spite-line-draw
    ;> Arguments:
    ;> - line (geometry-line) Line you want to draw
    ;> - size (number) How big the line should be
    ;> - color (color) The color of the line
    (define spite-line-draw
      (lambda (line size color)
        (let ((x1 (geometry-point-x-get (geometry-line-a-get line)))
              (y1 (geometry-point-y-get (geometry-line-a-get line)))
              (x2 (geometry-point-x-get (geometry-line-b-get line)))
              (y2 (geometry-point-y-get (geometry-line-b-get line))))
          (pffi-call sdl2*
                     'SDL_SetRenderDrawColor
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (color-red-get color))
                       (cons 'int (color-green-get color))
                       (cons 'int (color-blue-get color))
                       (cons 'int (color-alpha-get color))))
          (pffi-call sdl2*
                     'SDL_RenderSetScale
                     'int
                     (list (cons 'pointer renderer*)
                           (cons 'float (inexact (/ size 1)))
                           (cons 'float (inexact (/ size 1)))))
          (pffi-call sdl2*
                     'SDL_RenderDrawLine
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (exact (round (/ x1 size))))
                       (cons 'int (exact (round (/ y1 size))))
                       (cons 'int (exact (round (/ x2 size))))
                       (cons 'int (exact (round (/ y2 size))))))
          (pffi-call sdl2*
                     'SDL_RenderSetScale
                     'int
                     (list (cons 'pointer renderer*)
                           (cons 'float scale-x)
                           (cons 'float scale-y))))))

    ;> ### spite-rectangle-draw
    ;>
    ;> Arguments:
    ;> - rectangle (geometry-rectangle)
    ;> - size (number) The size of line used to draw the rectangle
    ;> - color (color)
    (define spite-rectangle-draw
      (lambda (rectangle size color)
        (let ((points (list (geometry-rectangle-b-get rectangle)
                            (geometry-rectangle-c-get rectangle)
                            (geometry-rectangle-d-get rectangle)))
              (start-point (geometry-rectangle-a-get rectangle))
              (end-point (geometry-rectangle-d-get rectangle))
              (previous-point (geometry-rectangle-a-get rectangle)))
          (pffi-call sdl2*
                     'SDL_SetRenderDrawColor
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (color-red-get color))
                       (cons 'int (color-green-get color))
                       (cons 'int (color-blue-get color))
                       (cons 'int (color-alpha-get color))))
          (pffi-call sdl2*
                     'SDL_RenderSetScale
                     'int
                     (list (cons 'pointer renderer*)
                           (cons 'float (inexact (/ size 1)))
                           (cons 'float (inexact (/ size 1)))))
          (for-each
            (lambda (point)
              (pffi-call sdl2*
                         'SDL_RenderDrawLine
                         'int
                         (list
                           (cons 'pointer renderer*)
                           (cons 'int (exact (round (/ (geometry-point-x-get previous-point) size))))
                           (cons 'int (exact (round (/ (geometry-point-y-get previous-point) size))))
                           (cons 'int (exact (round (/ (geometry-point-x-get point) size))))
                           (cons 'int (exact (round (/ (geometry-point-y-get point) size))))))
              (set! previous-point point))
            points)
          (pffi-call sdl2*
                     'SDL_RenderDrawLine
                     'int
                     (list
                       (cons 'pointer renderer*)
                       (cons 'int (exact (round (/ (geometry-point-x-get end-point) size))))
                       (cons 'int (exact (round (/ (geometry-point-y-get end-point) size))))
                       (cons 'int (exact (round (/ (geometry-point-x-get start-point) size))))
                       (cons 'int (exact (round (/ (geometry-point-y-get start-point) size))))))
          (pffi-call sdl2*
                     'SDL_RenderSetScale
                     'int
                     (list (cons 'pointer renderer*)
                           (cons 'float scale-x)
                           (cons 'float scale-y))))))

    ;> ### spite-font-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>   - Path to the font file you want to load
    ;> - size (number)
    ;>   - The size the font should be
    ;>
    ;> Returns: (number) Index of the loaded font
    ;>
    ;> Supports any font format SDL_format supports
    ;> [https://wiki.libsdl.org/SDL2_ttf/FrontPage](https://wiki.libsdl.org/SDL2_ttf/FrontPage)
    (define spite-font-load
      (lambda (path size)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load font, no such file: " path)))
        (set! fonts
          (vector-append
            fonts
            (vector (pffi-call sdl2-ttf*
                               'TTF_OpenFont
                               'pointer
                               (list (cons 'pointer (pffi-string->pointer path))
                                     (cons 'int size))))))
        (- (vector-length fonts) 1)))

    ;> ### spite-text-draw
    ;>
    ;> Arguments:
    ;>
    ;> - text (string)
    ;>   - Text you want to draw
    ;> - x (number)
    ;>   - Left top corner x position of the text inside the window
    ;> - y (number)
    ;>   - Left top corner y position of the text inside the window
    ;> - font (number)
    ;>   - Font index returned by spite-font-load
    ;> - color (vector (number) (number) (number))
    ;>   - Vector consisting of three numbers red, green and blue between 0-255
    (define spite-text-draw
      (lambda (text x y font color)
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 0) (color-red-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 1) (color-green-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 2) (color-blue-get color))
        (pffi-pointer-set! draw-text-color* 'uint8 (* (pffi-size-of 'uint8) 3) (color-alpha-get color))
        (let* ((surface (pffi-call sdl2-ttf*
                                   'TTF_RenderUTF8_Solid
                                   'pointer
                                   (list (cons 'pointer (vector-ref fonts font))
                                         (cons 'pointer (pffi-string->pointer (string-copy text)))
                                         (cons 'pointer (pffi-pointer-deref draw-text-color*)))))
               (texture (pffi-call sdl2*
                                   'SDL_CreateTextureFromSurface
                                   'pointer
                                   (list (cons 'pointer renderer*)
                                         (cons 'pointer surface)))))
          (pffi-call sdl2*
                     'SDL_QueryTexture
                     'int
                     (list (cons 'pointer texture)
                           (cons 'pointer null*)
                           (cons 'pointer null*)
                           (cons 'pointer draw-text-text-width*)
                           (cons 'pointer draw-text-text-height*)))
          (pffi-pointer-set! draw-text-draw-rect* 'int (* (pffi-size-of 'int) 0) x)
          (pffi-pointer-set! draw-text-draw-rect* 'int (* (pffi-size-of 'int) 1) x)
          (pffi-pointer-set! draw-text-draw-rect*
                             'int
                             (* (pffi-size-of 'int) 2)
                             (pffi-pointer-get draw-text-text-width* 'int 0))
          (pffi-pointer-set! draw-text-draw-rect*
                             'int
                             (* (pffi-size-of 'int) 3)
                             (pffi-pointer-get draw-text-text-height* 'int 0))
          (pffi-call sdl2*
                     'SDL_RenderCopy
                     'int
                     (list (cons 'pointer renderer*)
                           (cons 'pointer texture)
                           (cons 'pointer null*)
                           (cons 'pointer draw-text-draw-rect*))))))

    ;> ### spite-audio-load
    ;>
    ;> Arguments:
    ;>
    ;> - path (string)
    ;>  - Path to the audio file
    ;>
    ;> Returns: (number) Index of loaded audio
    ;>
    ;> Supports any format supported by
    ;> [https://www.libsdl.org/projects/old/SDL_mixer/](https://www.libsdl.org/projects/old/SDL_mixer/)
    (define spite-audio-load
      (lambda (path)
        (if (not (string? path)) (error "Load path must be string" path))
        (if (not (file-exists? path)) (error (string-append "Could not load audio, no such file: " path)))
        (set! audios
          (vector-append
            audios
            (vector (pffi-call sdl2-mixer*
                               'Mix_LoadWAV
                               'pointer
                               (list (cons 'pointer (pffi-string->pointer path)))))))
        (- (vector-length audios) 1)))

    ;> ### spite-audio-play
    ;>
    ;> Arguments:
    ;>
    ;> - audio-index (number)
    ;>  - Index of the audio returned by spite-audio-load
    (define spite-audio-play
      (lambda (audio-index)
        (pffi-call sdl2-mixer*
                   'Mix_PlayChannel
                   'int
                   (list (cons 'int -1)
                         (cons 'pointer (vector-ref audios audio-index))
                         (cons 'int 0)))))

    ;> ### spite-window-resizing-allow
    ;>
    ;> By default resizing is not allowed
    (define spite-window-resizing-allow
      (lambda ()
        (pffi-call sdl2*
                   'SDL_SetWindowResizable
                   'void
                   (list (cons 'pointer window*)
                         (cons 'int 1)))))

    ;> ### spite-window-resizing-disallow
    ;>
    ;> By default resizing is not allowed
    (define spite-window-resizing-disallow
      (lambda ()
        (pffi-call sdl2*
                   'SDL_SetWindowResizable
                   'void
                   (list (cons 'pointer window*)
                         (cons 'int 0)))))

    ;> ### spite-renderer-size-set
    ;>
    ;> Arguments:
    ;> - width (number) Set the renderer width
    ;> - height (number) Set the renderer height
    (define spite-renderer-size-set
      (lambda (width height)
        (pffi-call sdl2*
                   'SDL_RenderSetLogicalSize
                   'int
                   (list (cons 'pointer renderer*)
                         (cons 'int width)
                         (cons 'int height)))))

    ;> ### spite-renderer-scale-get
    ;>
    ;> Returns:
    ;>
    ;> - (list (cons 'x (number)) (cons 'y (number)))
    (define spite-renderer-scale-get
      (lambda ()
        (let ((x (pffi-pointer-allocate (pffi-size-of 'float)))
              (y (pffi-pointer-allocate (pffi-size-of 'float))))
          (pffi-call sdl2*
                     'SDL_RenderGetScale
                     'void
                     (list (cons 'pointer x)
                           (cons 'pointer y)))
          (list (cons 'x (pffi-pointer-get x 'float 0))
                (cons 'y (pffi-pointer-get y 'float 0))))))

    ;> ### spite-renderer-scale-set
    ;>
    ;> Arguments:
    ;>
    ;> - x (number)
    ;> - y (number)
    (define spite-renderer-scale-set
      (lambda (x y)
        (pffi-call sdl2*
                   'SDL_RenderSetScale
                   'void
                   (list (cons 'float (inexact (/ x 1)))
                         (cons 'float (inexact (/ y 1)))))))


    ;> ### spite-start
    ;>
    ;> Starts the game
    (define spite-start
      (lambda ()
        (cond
          ((not (spite-started?))
           (set! started? #t)
           (main-loop)))))

    ;> ### spite-reload-allow
    ;>
    ;> Arguments:
    ;> - key (string) The key you want to use for reload
    ;> - path (string) The path to your main file that you want to reload
    (define spite-reload-allow
      (lambda (key path)
        (set! reload-key key)
        (set! main-file-path path)))


    ;> ### spite-init
    ;>
    ;> Arguments:
    ;>
    ;> - title (string)
    ;>   - The window title
    ;> - widht (number)
    ;>   - Width of the window
    ;> - height
    ;>  - Height of the window
    ;>
    ;> Inits Spite and opens a window
    (define spite-init
      (lambda (title width height)
        (cond
          ((not (spite-started?))
           (set! sdl2* (pffi-shared-object-auto-load "SDL2" additional-load-paths))
           (set! sdl2-image* (pffi-shared-object-auto-load "SDL2_image" additional-load-paths))
           (set! sdl2-ttf* (pffi-shared-object-auto-load "SDL2_ttf" additional-load-paths))
           (set! sdl2-mixer* (pffi-shared-object-auto-load "SDL2_mixer" additional-load-paths))
           (pffi-call sdl2* 'SDL_Init 'int '((int . 32)))
           (pffi-call sdl2-ttf* 'TTF_Init 'int (list))
           (pffi-call sdl2-mixer* 'Mix_OpenAudio
                      'int
                      (list (cons 'int 44100)
                            (cons 'int 32784)
                            (cons 'int 2)
                            (cons 'int 2048)))

           (set! window* (pffi-call sdl2*
                                    'SDL_CreateWindow
                                    'pointer
                                    (list (cons 'pointer (pffi-string->pointer title))
                                          (cons 'int 0)
                                          (cons 'int 0)
                                          (cons 'int width)
                                          (cons 'int height)
                                          (cons 'int 4))))

           (set! renderer* (pffi-call sdl2*
                                      'SDL_CreateRenderer
                                      'pointer
                                      (list (cons 'pointer window*)
                                            (cons 'int -1)
                                            (cons 'int 2))))
           (pffi-call sdl2*
                      'SDL_RenderSetLogicalSize
                      'int
                      (list (cons 'pointer renderer*)
                            (cons 'int width)
                            (cons 'int height)))
           (pffi-call sdl2*
                      'SDL_RenderSetIntegerScale
                      'int
                      (list (cons 'pointer renderer*)
                            (cons 'int 1)))

           (render-clear)
           (render-present)))))

    ;> ### spite-started?
    ;>
    ;> Returns:
    ;> - (boolean) Returns #t if game is started
    (define spite-started?
      (lambda ()
        started?))

    ;> ### spite-reload
    ;>
    ;> Arguments:
    ;> - (string) Path to the main .scm file you want to reload
    (define spite-reload
      (lambda (main-file-path)
        (set! error? #f)
        (load main-file-path)
        (event-push (event-make 'reload (list)))))))
