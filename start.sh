#!/usr/bin/env sh

sash -r7 -L . -L ./schubert -e '(import (scheme load)) (load "init.scm") (load "main.scm")'
