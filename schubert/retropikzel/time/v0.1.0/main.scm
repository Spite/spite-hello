(define-library
  (retropikzel time v0.1.0 main)
  (import (scheme base)
          (scheme write)
          (scheme time))
  (export time-as-milliseconds
          time-as-seconds
          time-update
          time-timeout
          time-interval
          )
  (begin


    (define-record-type timeout
      (make-timeout index time proc)
      timeout?
      (index timeout-index)
      (time timeout-time)
      (proc timeout-proc))

    (define-record-type interval
      (make-interval index time proc milliseconds)
      interval?
      (index interval-index)
      (time interval-time interval-time-set!)
      (proc interval-proc)
      (milliseconds interval-milliseconds))

    (define queue (vector))

    (define time-as-seconds
      (lambda ()
        (/ (current-jiffy) (jiffies-per-second))))

    (define time-as-milliseconds
      (lambda ()
        (* (time-as-seconds) 1000)))

    (define time-update
      (lambda ()
        (let ((queue-copy (vector-copy queue))
              (current-time (time-as-milliseconds)))

          (set! queue (vector))

          (vector-for-each
            (lambda (item)
              (cond
                ((timeout? item)
                 (if (< (timeout-time item) current-time)
                   (apply (timeout-proc item) (list))
                   (set! queue (vector-append queue (vector item)))))
                ((interval? item)
                 (if (< (interval-time item) current-time)
                   (begin
                     (apply (interval-proc item) (list))
                     (interval-time-set! item (+ current-time
                                                 (interval-milliseconds item)))))
                 (set! queue (vector-append queue (vector item))))))
            queue-copy))))

    (define time-timeout
      (lambda (proc milliseconds)
        (let ((index (vector-length queue)))
          (set! queue
            (vector-append
              queue
              (vector (make-timeout index
                                    (+ (time-as-milliseconds) milliseconds)
                                    proc))))
          index)))

    (define time-interval
      (lambda (proc milliseconds)
        (let ((index (vector-length queue)))
          (set! queue
            (vector-append
              queue
              (vector (make-interval index
                                     (+ (time-as-milliseconds) milliseconds)
                                     proc
                                     milliseconds))))
          index)))

    (define time-stop
      (lambda (index)
        (vector-set! queue index (list))))

    ))
