


(define handle-key-down
  (lambda (data)
    (let ((key (cdr (assoc 'key data))))
      (cond
        ((string=? key "Space")
         (event-push (event-make 'hello (list "I am user defined event"))))
        ((string=? key "W") (set! logo-y (- logo-y 5)))
        ((string=? key "A") (set! logo-x (- logo-x 5)))
        ((string=? key "S") (set! logo-y (+ logo-y 5)))
        ((string=? key "D") (set! logo-x (+ logo-x 5)))
        ))))

(define handle-hello
  (lambda (message)
    (display message)
    (newline)
    (spite-audio-play click)))

(spite-update
  (lambda (events)
    (vector-for-each
      (lambda (event)
        (let ((type (event-type-get event))
              (data (event-data-get event)))
          (cond
            ((equal? type 'key-down) (handle-key-down data))
            ((equal? type 'hello) (handle-hello data)))))
      events)))




(spite-draw
  (lambda ()
    (spite-image-draw rainbow-lambda logo-x logo-y 32 32)
    (spite-text-draw "Hello world" 50 50 font black)

    ))

(spite-start)
