Example starting point for [Spite](https://codeberg.org/Spite/spite).


## Running

### With Sagittarius

Run by running either start.sh or start.bat depending on if you are on Unix or
Windows.

### With Kawa

Run by running either start-java.sh or start-java.bat depending on if you are on Unix or
Windows.


## How to "Play"

Use WASD to move and Space to play sound effect.

Example stuff is made by:

RETRO_SPACE.ttf by [Dizzy Crow](https://opengameart.org/users/dizzy-crow)
[https://opengameart.org/content/font-retrospace](https://opengameart.org/content/font-retrospace)


rainbow-lambda-logo.png by
[ramin_hal9001](https://opengameart.org/users/raminhal9001)


click.wav by [qubodup](https://opengameart.org/users/qubodup)
[https://opengameart.org/content/click](https://opengameart.org/content/click)

## System requirements

### Windows

Everything needed is included

### Unix


#### Sagittarius

On Debian and derivatives (Ubuntu/Mint...) download the latest source from
[https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/](https://bitbucket.org/ktakashi/sagittarius-scheme/downloads/)
unpack it and then run

    cd <sagittarius-directory>
    apt install cmake libgc-dev zlib1g-dev libffi-dev libssl-dev
    mkdir build
    cd build
    cmake ..
    make
    make install


#### SDL2

You need sdl2, sdl2-image, sdl2-ttf and sdl2-mixer.

On Debian/Ubuntu you can install them with

    apt install libsdl2-2.0.0 libsdl2-image-2.00 libsdl2-ttf-2.0.0
    libsdl2-mixer-2.0.0

On guix you can run:

    guix shell -m manifest.scm

to get shell with needed SDL2 dependencies




