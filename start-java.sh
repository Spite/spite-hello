#!/usr/bin/env sh

java \
    --add-exports java.base/jdk.internal.foreign.abi=ALL-UNNAMED \
    --add-exports java.base/jdk.internal.foreign.layout=ALL-UNNAMED \
    --add-exports java.base/jdk.internal.foreign=ALL-UNNAMED \
    --enable-native-access=ALL-UNNAMED \
    --enable-preview \
    -jar kawa.jar \
    --r7rs \
    --full-tailcalls \
    -Dkawa.import.path=".:./schubert" \
    -e '(load "init.scm") (load "main.scm")'
